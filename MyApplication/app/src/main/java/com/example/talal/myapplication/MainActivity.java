package com.example.talal.myapplication;


import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.example.talal.myapplication.adapter.PostLocation;
import com.example.talal.myapplication.displayApp.DisplayAppFragment;
import com.example.talal.myapplication.myApp.MyAppFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener{

    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private BeaconManager beaconManager;
    private static final Region region = new Region("regionId", ESTIMOTE_PROXIMITY_UUID, null, null);


    @Override
    public void onCreate(  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setBluetooth(true);
        // Set up the action bar.
        final   ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#3D4C4C")));
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#526666")));
        // For each of the sections in the app, add a tab to the action bar.
        actionBar.addTab(actionBar.newTab().setText("Display Applications").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("My Applications").setTabListener(this));

        // Monitor beacon

        beaconManager = new BeaconManager(this);
        beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 0);


        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> beacons) {
                sendPreferences();
                Log.v("Entered region","+++++++++++++++++++++++++++++++++++++++++++++");
            }

            @Override
            public void onExitedRegion(Region region) {Log.v("Exited region","---------------------------------------------------------------------");
                emptyPost();
            }
        });

        // check if there is a beacon by ranging
        // check if there is a beacon
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                Log.d(TAG, "Ranged beacons: " + beacons);
                if (!beacons.isEmpty()) {
                    sendPreferences();
                } else {
                    emptyPost();
                }

            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(region);
                } catch (RemoteException e) {
                    Log.e(TAG, "Cannot start ranging", e);
                }
            }
        });

    }

    protected void onStop() {
        super.onStop();
        try {
            beaconManager.stopRanging(region);
        } catch (RemoteException e) {
            Log.e(TAG, "Cannot stop but it does not matter now", e);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();


        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startMonitoring(region);
                } catch (RemoteException e) {
                    Log.d(TAG, "Error while starting monitoring");
                }
            }
        });
    }



    @Override
    protected void onDestroy() {

        beaconManager.disconnect();
        super.onDestroy();

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {

            getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                creatMap();
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        /**
         * On first tab we will show our Apllication list
         *
         */



        if (tab.getPosition() == 0) {
            android.support.v4.app.Fragment favFragment = getSupportFragmentManager().findFragmentByTag("FavList") ;
            android.support.v4.app.Fragment parFragment = getSupportFragmentManager().findFragmentByTag("Parameter") ;
            getSupportFragmentManager().popBackStack("PAR_FR",FragmentManager.POP_BACK_STACK_INCLUSIVE);

            if (favFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(favFragment).commit();
            }
            if (parFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(parFragment).commit();
            }
//
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new DisplayAppFragment(), "AppList").commit();


        }

        /**
         * On second tab we will show MyApplication list
         *
         */
        else if (tab.getPosition() == 1) {
            getSupportFragmentManager().popBackStack("webFr", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MyAppFragment(), "FavList").commit();
        }

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }
    // create googleMap activity
    private void creatMap() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }


    // send the preferences to displays server
    private void sendPreferences(){
        JSONObject json =new JSONObject();
       // Toast.makeText(this, "Display preferences sent", Toast.LENGTH_SHORT).show();
        Map<String, ?> allEntries = this.getSharedPreferences("favSetting", Context.MODE_PRIVATE).getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            try {
                json.put(entry.getKey(),entry.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //add display ID
        try {
            json.put("id","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        PostLocation pl = new PostLocation(this, json);
        pl.execute();
    }


    private void emptyPost(){
        JSONObject json =new JSONObject();
        try {
            json.put("id","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PostLocation pl = new PostLocation(this, json);
        pl.execute();
    }

    // activate Bluetooth
    private static boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }
        // No need to change bluetooth state
        return true;
    }

}

