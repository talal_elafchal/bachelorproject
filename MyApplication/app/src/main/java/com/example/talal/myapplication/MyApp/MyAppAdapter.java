package com.example.talal.myapplication.myApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.talal.myapplication.R;

import java.util.List;
import java.util.Map;

class MyAppAdapter extends ArrayAdapter<MyAppForm> {

    private final ImageLoader mImageLoader;



    public MyAppAdapter(Context context) {
        super(context, R.layout.fav_list_item);

        mImageLoader = new ImageLoader(VolleyApplication.getInstance().getRequestQueue(), new BitmapLruCache());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fav_list_item, parent, false);
        }

        final MyAppForm favItem = getItem(position);
        // NOTE: You would normally use the ViewHolder pattern here
        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.favimage);
        TextView textView = (TextView) convertView.findViewById(R.id.textfav);



        //shared file for on/of setting
        final SharedPreferences sharedPref = convertView.getContext().getSharedPreferences("favSetting", Context.MODE_PRIVATE);


        ToggleButton toogle = (ToggleButton) convertView.findViewById(R.id.switchbutton);
        toogle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(favItem.getTitle(), favItem.getTitle());
                    editor.commit();
                    // The toggle is enabled
                    Map<String, ?> allEntries = getContext().getSharedPreferences("favSetting", Context.MODE_PRIVATE).getAll();
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                    }

                } else {

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.remove(favItem.getTitle());
                    editor.commit();
                    Map<String, ?> allEntries = getContext().getSharedPreferences("favSetting", Context.MODE_PRIVATE).getAll();
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                    }

                    // The toggle is disabled
                }
            }
        });




        SharedPreferences getPref = convertView.getContext().getSharedPreferences("favSetting", Context.MODE_PRIVATE);
        String  checkElement = getPref.getString(favItem.getTitle() ,"Empty");

        if(checkElement.equals("Empty")){

            toogle.setChecked(false);

        }
        else {

            toogle.setChecked(true);}


        imageView.setImageUrl(favItem.getIconUrl(), mImageLoader);
        textView.setText(favItem.getTitle());


        return convertView;
    }

    public void swapImageRecords(List<MyAppForm> objects) {
        clear();

        for(MyAppForm object : objects) {
            add(object);
        }

        notifyDataSetChanged();
    }
}
