package com.example.talal.myapplication.myApp;

import java.lang.String;
class MyAppForm {
    private final String url;
    private final String title;
    private final String parameter1;
    private final String parameter2;



    public MyAppForm(String url, String title, String parameter1, String parameter2) {
        this.url = url;
        this.title = title;
        this.parameter1 = parameter1;
        this.parameter2= parameter2;



    }

    public String getParameter1() {
        return parameter1;
    }
    public String getParameter2() {
        return parameter2;
    }
    public String getTitle() {
        return title;
    }

    public String getIconUrl() {
        return url;
    }
}
