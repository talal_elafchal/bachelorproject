package com.example.talal.myapplication.myApp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.talal.myapplication.R;
import com.example.talal.myapplication.adapter.PostLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
     * A placeholder fragment containing a simple view.
     */
    public class MyAppFragment extends Fragment {

        private MyAppAdapter mAdapter;
        private Activity mActivity;


    public MyAppFragment() {
        }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            return inflater.inflate(R.layout.fragment_app, container, false);
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            mAdapter = new MyAppAdapter(getActivity());
            Button button= (Button) getView().findViewById(R.id.setButton);

            //on click button Set
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    JSONObject json =new JSONObject();
                    // Perform action on click
                    Toast.makeText(getActivity(), "Display preferences sent", Toast.LENGTH_SHORT).show();
                    Map<String, ?>allEntries = mActivity.getSharedPreferences("favSetting", Context.MODE_PRIVATE).getAll();
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        try {
                            json.put(entry.getKey(),entry.getValue());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                       // Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                    }

                    PostLocation pl = new PostLocation(getActivity(), json);
    				pl.execute();

//                    allEntries = mActivity.getSharedPreferences("favSetting", Context.MODE_PRIVATE).getAll();
//                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
//                        Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
//                    }

                }
            });


            fetch();
            final ListView listView = (ListView) getView().findViewById(R.id.list1);
            listView.setAdapter(mAdapter);

//
            //onclick
            listView.setClickable(true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    MyAppForm a = (MyAppForm) listView.getItemAtPosition(position);
                    String parameter1 = a.getParameter1();
                    String parameter2 = a.getParameter2();
                    Bundle bundle = new Bundle();
                    bundle.putString("parameter1", parameter1);
                    bundle.putString("parameter2", parameter2);
                    bundle.putString("title", a.getTitle());
                    Fragment fragment = new ParameterFragment();
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack("PAR_FR");
                    fragmentTransaction.commit();

                }
            });


        }




        private void fetch() {
            JsonObjectRequest request = new JsonObjectRequest(
                    "http://uc-dev.inf.usi.ch:9031/assets/json/jobject.json",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
                                List<MyAppForm> imageRecords = parse(jsonObject);

                                mAdapter.swapImageRecords(imageRecords);
                            }
                            catch(JSONException e) {
                                Toast.makeText(getActivity(), "Unable to parse data: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getActivity(), "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

            VolleyApplication.getInstance().getRequestQueue().add(request);
        }

        private List<MyAppForm> parse(JSONObject json) throws JSONException {
            ArrayList<MyAppForm> records = new ArrayList<MyAppForm>();

            JSONArray jsonFav = json.getJSONArray("applications");

            for(int i =0; i < jsonFav.length(); i++) {
                JSONObject jsonImage = jsonFav.getJSONObject(i);
                String title = jsonImage.getString("title");
                String url = jsonImage.getString("icon");
                String parameter1 = jsonImage.getString("parameter1");
                String parameter2 = jsonImage.getString("parameter2");


                if (checkifAppisInstalled(title)){
                    MyAppForm record = new MyAppForm(url, title,parameter1,parameter2);
                    records.add(record);
                }
            }

            return records;
        }

        private Boolean checkifAppisInstalled(String title){
            SharedPreferences getPref = mActivity.getSharedPreferences("displayApp", Context.MODE_PRIVATE);
            Map<String, ?> allEntries = getPref.getAll();
            Set<String> s =  allEntries.keySet();
            return s.contains(title);

        }



    }

