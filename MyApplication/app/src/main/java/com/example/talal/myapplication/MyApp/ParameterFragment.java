package com.example.talal.myapplication.myApp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.talal.myapplication.R;



/**
 * Created by talal on 30/04/15.
 */
public class ParameterFragment extends Fragment {

    private Activity mActivity;
    private String parameter1 = "";
    private String parameter2 = "";
    private String title ="";
    private SharedPreferences getPrefPar;
    private LinearLayout layout;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        this.layout = (LinearLayout) getView().findViewById(R.id.parameter);
        populate();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        getPrefPar = mActivity.getSharedPreferences("favSetting", Context.MODE_PRIVATE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String parameter1 = this.getArguments().getString("parameter1");
        String parameter2 = this.getArguments().getString("parameter2");
        String title = this.getArguments().getString("title");
        if (parameter1 != null){this.parameter1 = parameter1;}
        if (parameter2 != null){this.parameter2 = parameter2;}
        if (parameter2 != null){this.title = title;}


        View v = inflater.inflate(R.layout.parameter_fragment, container, false);
        return v;
    }

    // create views in the fragment
    private void populate() {
        if (parameter1.equals("NumberPicker")|| parameter2.equals("NumberPicker")){
            createNumberPicker();
        }
        if(parameter1.equals("EditText")||parameter2.equals("EditText")) {
          createEditText();
        }
    }

    private void createNumberPicker(){

        NumberPicker numberPicker = new NumberPicker(this.getActivity());
        final RadioGroup radioGroup = new RadioGroup(this.getActivity());
        final RadioButton viganello = new RadioButton(this.getActivity());
        viganello.setText("Viganello");
        final RadioButton lamone = new RadioButton(this.getActivity());
        lamone.setText("Lamone");
        final RadioButton ffs = new RadioButton(this.getActivity());
        ffs.setText("Stazione");
        final RadioButton cornaredo = new RadioButton(this.getActivity());
        cornaredo.setText("Cornaredo");



        viganello.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = getPrefPar.edit();
                editor.putString((title + "direction"), viganello.getText().toString());
                editor.commit();
                Log.v("my button", viganello.getText().toString());
            }
        });

        lamone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = getPrefPar.edit();
                editor.putString((title + "direction"), lamone.getText().toString());
                editor.commit();
                Log.v("my button", lamone.getText().toString());
            }
        });

        ffs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = getPrefPar.edit();
                editor.putString((title + "direction"), ffs.getText().toString());
                editor.commit();
                Log.v("my button", ffs.getText().toString());
            }
        });

        cornaredo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getPrefPar.edit();
                editor.putString((title + "direction"), cornaredo.getText().toString());
                editor.commit();
                Log.v("my button", cornaredo.getText().toString());
            }
        });
        numberPicker.setMinValue(5);
        numberPicker.setMaxValue(6);
        int saveValue  =getPrefPar.getInt((title + "number"), 5);



        numberPicker.setValue(saveValue);
        numberPicker.setWrapSelectorWheel(true);

        if (saveValue ==5){  radioGroup.removeAllViews();
            radioGroup.addView(lamone);
            radioGroup.addView(viganello);}
        if (saveValue ==6){  radioGroup.removeAllViews();
            radioGroup.addView(ffs);
            radioGroup.addView(cornaredo);}
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {


            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // TODO Auto-generated method stub
                radioGroup.clearCheck();
                if(newVal==5){
                    radioGroup.removeAllViews();
                    radioGroup.addView(lamone);
                    radioGroup.addView(viganello);
                }
                if(newVal==6){
                    radioGroup.removeAllViews();
                    radioGroup.addView(ffs);
                    radioGroup.addView(cornaredo);

                }
                SharedPreferences.Editor editor = getPrefPar.edit();
                editor.putInt((title+"number"), newVal);
                editor.commit();
            }
        });

        //for Demo Make button


        layout.addView(numberPicker);
        layout.addView(radioGroup);

    }


    private void createEditText(){
        final EditText editText = new EditText(this.getActivity());
        editText.setInputType(1);

        editText.setText(getPrefPar.getString((title + "direction"), ""));
        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                // if keydown and "enter" is pressed
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    // display a floating message
                    Toast.makeText(getActivity(), editText.getText(), Toast.LENGTH_LONG).show();
                    String input = editText.getText().toString();
                    SharedPreferences.Editor editor = getPrefPar.edit();
                    editor.putString((title + "direction"), input);
                    editor.commit();
                    return true;

                }

                return false;
            }
        });
        //editText.requestFocus();
        layout.addView(editText);

    }



}
