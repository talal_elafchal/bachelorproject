package com.example.talal.myapplication.adapter;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by talal on 06/05/15.
 */
public class PostLocation extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "PostLocation";
    private final Context context;
    private final JSONObject json;

    public PostLocation(Context context, JSONObject json) {

        this.context = context;
        this.json = json;

    }

//http://httpbin.org/post
    @Override
    protected Void doInBackground(Void... params) {

        try {
            postToServer("http://uc-dev.inf.usi.ch:9031/mobile", json);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        // response
    }





    private void postToServer(String suffix, JSONObject json) {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost httpost = new HttpPost(suffix);



        StringEntity se = null;
        try {
            se = new StringEntity(json.toString());
            httpost.setEntity(se);
            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");

            ResponseHandler responseHandler = new BasicResponseHandler();
            System.out.println(httpclient.execute(httpost, responseHandler));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



