package com.example.talal.myapplication.displayApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.talal.myapplication.myApp.BitmapLruCache;
import com.example.talal.myapplication.myApp.VolleyApplication;
import com.example.talal.myapplication.R;

import java.util.List;

class DisplayAppAdapter extends ArrayAdapter<DisplayAppModel> {
    private final ImageLoader mImageLoader;



    public DisplayAppAdapter(Context context) {
        super(context, R.layout.app_list_item);

        mImageLoader = new ImageLoader(VolleyApplication.getInstance().getRequestQueue(), new BitmapLruCache());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SharedPreferences sharedPref;
        final DisplayAppModel appModel;
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.app_list_item, parent, false);
        }
        appModel = getItem(position);



        //Shared Preferences

         sharedPref = convertView.getContext().getSharedPreferences("displayApp", Context.MODE_PRIVATE);





        // use the ViewHolder pattern here

        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.appimage);
        TextView title = (TextView) convertView.findViewById(R.id.text1);
        final TextView info = (TextView) convertView.findViewById(R.id.info);


        Switch toggle = (Switch) convertView.findViewById(R.id.togglebutton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.remove(appModel.getTitle());
                    editor.commit();
                    info.setText("info ..");

                } else {

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(appModel.getTitle(),appModel.getTitle());
                    editor.commit();
                    info.setText("");
                    // The toggle is disabled
                }
            }
        });




        SharedPreferences getPref = convertView.getContext().getSharedPreferences("displayApp", Context.MODE_PRIVATE);
        String  checkElement = getPref.getString(appModel.getTitle() ,"Empty");

        if(checkElement.equals("Empty")){

           toggle.setChecked(true);
        }
        else {
            toggle.setChecked(false);}




        imageView.setImageUrl(appModel.getUrl(), mImageLoader);
        title.setText(appModel.getTitle());




        return convertView;
    }

    public void swapImageRecords(List<DisplayAppModel> objects) {
        clear();

        for(DisplayAppModel object : objects) {
            add(object);
        }

        notifyDataSetChanged();
    }
}
