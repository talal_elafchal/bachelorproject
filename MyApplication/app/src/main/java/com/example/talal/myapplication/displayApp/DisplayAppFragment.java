package com.example.talal.myapplication.displayApp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.talal.myapplication.myApp.VolleyApplication;
import com.example.talal.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class DisplayAppFragment extends Fragment {

    private DisplayAppAdapter mAdapter;
    private static final String url = "http://uc-dev.inf.usi.ch:9031/assets/json/jobject.json";



    public DisplayAppFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app, container, false);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new DisplayAppAdapter(getActivity());
        fetch();

        final ListView listView = (ListView) getView().findViewById(R.id.list1);
        listView.setAdapter(mAdapter);




        //onclick
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                DisplayAppModel a = (DisplayAppModel) listView.getItemAtPosition(position);
                String url = a.getPageUrl();
                Bundle bundle = new Bundle();
                bundle.putString("weburl", url);
                Fragment fragment = new InformationFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container , fragment);
                fragmentTransaction.addToBackStack("webFr");
                fragmentTransaction.commit();

            }
        });


    }

    private void fetch() {
        JsonObjectRequest request = new JsonObjectRequest(
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        try {
                            List<DisplayAppModel> appModel = parse(jsonObject);

                            mAdapter.swapImageRecords(appModel);
                        }
                        catch(JSONException e) {
                            Toast.makeText(getActivity(), "Unable to parse data: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(getActivity(), "Unable to fetch data: " + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        VolleyApplication.getInstance().getRequestQueue().add(request);
    }

    private List<DisplayAppModel> parse(JSONObject json) throws JSONException {
        ArrayList<DisplayAppModel> applications = new ArrayList<DisplayAppModel>();

        JSONArray jsonApplications = json.getJSONArray("applications");

        for(int i =0; i < jsonApplications.length(); i++) {
            JSONObject jsonApplication = jsonApplications.getJSONObject(i);
            String title = jsonApplication.getString("title");
            String url = jsonApplication.getString("icon");


            String pageUrl = jsonApplication.getString("url");


            DisplayAppModel application = new DisplayAppModel(url, title,pageUrl);
            applications.add(application);
        }

        return applications;
    }


}

