package com.example.talal.myapplication.displayApp;

class DisplayAppModel {
    private final String url;
    private final String title;
    private final String pageUrl;

    public DisplayAppModel(String url, String title, String pageUrl) {
        this.url = url;
        this.title = title;

        this.pageUrl = pageUrl;

    }

    public String getTitle() {
        return title;
    }

    public String getPageUrl(){return  pageUrl;}
    public String getUrl() {
        return url;
    }
}
