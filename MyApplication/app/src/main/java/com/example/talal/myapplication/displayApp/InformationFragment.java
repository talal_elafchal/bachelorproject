package com.example.talal.myapplication.displayApp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.talal.myapplication.R;

/**
 * Created by talal on 27/04/15.
 */
public class InformationFragment extends Fragment {





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String url = this.getArguments().getString("weburl");
        Log.v("weburl ",url);
        View v = inflater.inflate(R.layout.fragment_web_view, container, false);
        WebView webView = (WebView) v.findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        return v;
    }




}
