package controllers;



import play.libs.Json;
import play.data.Form;  
import views.html.*;
import java.util.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.Date;

                       




public class Application extends Controller {
	
	public static ObjectNode result = Json.newObject();
	
	
    public static Result index() {
        return ok(index.render("I-Display TPL View"));
    }
    
   

  
    public static Result getPreferences() {
    	
//    	ObjectNode result= Json.newObject();
    	result= Json.newObject();
    	 Map<String, String> mapParameters = Form.form().bindFromRequest().data();
    	 for(Map.Entry<String, String> entry : mapParameters.entrySet()){
                 result.put(entry.getKey(),entry.getValue());
    	 }
    	  Date date = new Date();
    	  String time =""+ date.getTime();
    	 result.put("date",time );
        
        return ok(result);
    }
    
    public static Result sendPreferences(){
    	return ok(result);
    }

}

    

