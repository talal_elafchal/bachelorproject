// This example is based on the tplsa source code (http://bs.tplsa.ch/rtpi.html)

var Items = [];
var tpl ="" ;
var date = 0;
var tplNumber = 0;
var direction= "";
var refreshInterval = 3000;

$(function(){
	loadTimeTable();
	getPreference();
	startTimeTableUpdate();
});

function Item(index, route, target, direction, destination, busNumber,image, time, predictable,remaining){
	this.index = index;
	this.routeUID = route;
	this.targetUID = target;
	this.dir = direction;
	this.dest = destination;
	this.busNumber = busNumber;
	this.img = image;
	this.time = time;
	this.pred = predictable;
	
}//Item 



$("#timeTablePage").mousedown(function(){//disable draging
	return false;
});//mousedown 



function CalculateRemainigTime(t){
	var date = new Date();
	var res = t.split(":");
	var newDate = new Date();
	newDate.setHours(res[0]);
	newDate.setMinutes(res[1]);
	var remaining = ( Math.abs(newDate.getTime() - date.getTime()))/60000;
	return Math.floor(remaining);
}

function startTimeTableUpdate(){
	refreshTimer = setInterval(function(){
		var date = new Date();
		loadTimeTable();
		getPreference();
		var h = date.getHours(); if(h<10) h = "0"+h;
		var m = date.getMinutes(); if(m<10) m = "0"+m;
		$("#footer-update").text("Updated at "+h+":"+m);
	}, refreshInterval);    
}//startTimeTableUpdate 

var loadTimeTable = function(){
    $.ajax({
        type: 'GET',
        url: "http://bs.tplsa.ch/RTPI/rtpi?data=%7Bsource%3A%22F4C26F1B178BA79C96903E4DE4BD4BF5%22%2Cdestination%3A0%2Ctype%3AGETPREDICTIONS%2Cdata%3A%7Bparameters%3A%5B338%2C339%5D%7D%7D&_=1425029568831",
        dataType: 'json',
        success: function(data){
        	if (data.data.length != 0) {
	        	$.each(data.data, function(i, item){
	        		//console.log(i+" : "+ item.Route+" "+item.Target+" "+item.Dir+" "+item.Dest+" "+item.Img+" "+item.Time+" "+item.RouteCode);
	        		Items[i] = new Item(i, item.Route, item.Target, item.Dir, item.Dest, item.RouteCode,item.Img, item.Time, item.Pred);
	        	});//each
	        	updateTimeTable();
	        }//if
        }//success 
    });//ajax 
};//loadtimetable 




var getPreference = function(){
	console.log("getp");
	$.ajax({
        type: 'GET',
        url: "http://uc-dev.inf.usi.ch:9031/mApp",
        dataType: 'json',
        success: function(data){
        	if (data.length != 0) {
	        	console.log(data);//each
	        	date = data.date;
	        	tpl = data.Tpl;
	        	direction =data.Tpldirection;
	        	tplNumber = data.Tplnumber;
	        	//console.log(tpl,date,tplNumber);
	        	calculatePassedTime();
	        	
	        }//if
        }//success 
    });//ajax 
};


function sortItems(a,b){
    return ((a.time < b.time) ? -1 : (a.time > b.time) ? 1 : 0);
}//sortItineraries 

function updateTimeTable(){
   // Clear the list
   $('#timeTableList').empty();
   
   // Sort the list
   Items.sort(sortItems);
 
   // Append items
   for (var i=0; i<Items.length; i++) {
   	  var info = Items[i].time;
   	  var color = 'black';
   	  var color2 = 'black';
   	  var backgroundColor = 'white'
   	  var remaining = CalculateRemainigTime(info) ; 
   	  if(remaining < 1){
   		  remaining = "Now";
   		  color2 = "red";
   	  }
   	 else if(remaining < 3){
   		color2 = "red";
   		remaining = remaining + " '";
   	}
   	else{
   		remaining = remaining + " '"
   	}
   	  if (Items[i].pred == 0) {
   	 		info = "RIT: delayed due to traffic jams."; 
   	 		color = "red";  
   	  }//if
   	  //number selected from mobule app
   	  if (tpl=="Tpl" && Items[i].busNumber == tplNumber &&  (Items[i].dest.toUpperCase().indexOf(direction.toUpperCase())!== -1 ){
   		  backgroundColor = 'aqua';
   	  }
   	  console.log(calculatePassedTime());
   	  
      var timeTableListItem = '<li class="timeTableListItem" style="padding: 0px;" record_id='+ Items[i].index +'><div class=""" style = "background:'+backgroundColor+';"><img src="http://bs.tplsa.ch/images/routes/'+ Items[i].img + '" style="border-radius:10px; position:relative; top:4px; left:4px;"> <h1 style = "padding-left:100px; color:'+ color2 +';"> '+ remaining +' </h1> <h1 style="position: absolute; top: 18px; left: 100px;">' + Items[i].dest.toUpperCase() + '</h1><p> </p><p class=""> <h3 style="color:'+color+';position: absolute;left: 100px;top: 40px;"><strong>' + info + '</strong></h3></p></div></li>';
      // Add item to list
      $('#timeTableList').append(timeTableListItem);
   }//for
   
   // Update the list
   $('#timeTableList').listview('refresh');
}//updateTimeTable 

function calculatePassedTime(){
	var d = new Date().getTime();
	var remaining = Math.floor((d- date)/60000);
	console.log("remaining",remaining);
	return remaining;
}
