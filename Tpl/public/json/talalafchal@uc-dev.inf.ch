{"applications":[   
	{
        "title": "Tpl",
    	"icon": "http://uc-dev.inf.usi.ch:9030/assets/images/tpl.png",
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"NumberPicker",
        "parameter2":"EditText"
    },
    {
        "title": "Twitter",
        "icon": "http://pdnet.inf.unisi.ch/usiapps/Twitter/appicon/appicon",       
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"EditText",
        "parameter2":""
    },
    {
        "title": "Images",
        "icon": "http://pdnet.inf.unisi.ch:9004/assets/images/mg_logo_icon.png",
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"",
        "parameter2":""
    },
    {
        "title": "Calendar",
        "icon": "http://pdnet.inf.unisi.ch/usiapps/USI%20Academic%20Calendar/appicon/appicon",
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"",
        "parameter2":""
    },
    {
        "title": "News",
        "icon": "http://pdnet.inf.unisi.ch/usiapps/USI%20News%20and%20Events/appicon/appicon",
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"",
        "parameter2":""
    },
    {
        "title": "Housing",
        "icon": "http://pdnet.inf.unisi.ch/usiapps/USI%20Housing/appicon/appicon",
        "url": "http://uc-dev.inf.usi.ch:9030/",
        "parameter1":"",
        "parameter2":"NumberPicker"
    }
    ]
}